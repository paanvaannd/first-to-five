#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Handles creation, loading, & modification of game_data.db."""

# Import built-in libraries
from __future__ import annotations
from typing import Optional, Union, Any
import os.path
import sqlite3
import sys

# Import local modules
from players import Player


def checkExists(table: str) -> bool:
    """
    Check if specified table exists.

    Parameters
    ----------
    table: str
        Name of table thats existence will be checked.

    Returns
    -------
    bool
        Returns True if table exists; otherwise, returns False.
    """
    with connection:
        cursor.execute(f"SELECT EXISTS (SELECT * FROM {table})")
        return False if cursor.fetchone()[0] == 0 else True


def createDatabase() -> None:
    """Create & populates database."""
    _connection: sqlite3.Connection = sqlite3.connect(file_name)
    with _connection:
        _cursor: sqlite3.Cursor = _connection.cursor()
        _cursor.execute("""CREATE TABLE Players (
                           name     text,
                           type     text)""")

        _cursor.execute("""CREATE TABLE Save (
                           name         text,
                           score        int,
                           last_guess   int)""")

        _cursor.execute("""CREATE TABLE History (
                           turn     int,
                           name     text,
                           guess    int)""")

        _cursor.execute("""CREATE TABLE Statistics (
                           turn     int,
                           name     text,
                           mean     int,
                           median   int,
                           mode     int)""")

        _cursor.execute("""CREATE TABLE Outcome (
                           turn     int,
                           winner   text,
                           gain     int)""")

        _connection.commit()


def loadDatabase() -> tuple[sqlite3.Connection, sqlite3.Cursor]:
    """
    Establish connection to SQLite database.

    Returns
    -------
    tuple
        Tuple containing connection & cursor objects for game_data.db.
    """
    _connection: sqlite3.Connection = sqlite3.connect(file_name)
    _cursor: sqlite3.Cursor = _connection.cursor()
    return (_connection, _cursor)
    

def fetchData(name: str) -> list[int]:
    """
    Fetch data for statistical analysis.

    Parameters
    ----------
    name : str
        Name by which the player is referred.

    Returns
    -------
    data : list[int]
        Player's previous, recorded guesses.
    """
    with connection:
        data: list = []
        result: int
        for result in cursor.execute("""SELECT guess FROM History
                                        WHERE name = ?""", (name,)):
            data.append(result)
        print(f"The guesses for {name} are:\n{data}")
        return data


def loadStats(player: Player) -> tuple[str, float, float, float]:
    """
    Fetch most recent database statistics values.

    Parameters
    ----------
    player : Player
        One of the game's players.

    Returns
    -------
    tuple[str, float, float, float]
        Last calculated mean, median, & mode for a player.
    """
    with connection:
        cursor.execute("""SELECT name, mean, median, mode FROM Statistics
                          WHERE name = ? ORDER BY turn DESC LIMIT 1""",
                       (getattr(player, 'name'),))
        return cursor.fetchone()


def recreatePlayerTable(player: Player) -> None:
    """
    Populate 'Players' table with player name & species.

    Parameters
    ----------
    player : Player
        One of the game's players.
    """
    with connection:
        cursor.execute("INSERT INTO Players VALUES (?, ?)",
                       (getattr(player, 'name'),
                        getattr(player, 'species')))
        connection.commit()


def checkPlayerTable(player_list: list[Player]) -> bool:
    """
    Compare names of current players to 'Players' table values.

    Parameters
    ----------
    player_list : list[Player]
        List of Player instances, 1 per player defined in config.json.

    Returns
    -------
    bool
        Returns True if player's name exists in database; else, False.
    """
    names: list = []
    with connection:
        cursor.execute("SELECT DISTINCT name FROM Players")
        row: tuple[str, None]
        for row in cursor.fetchall():
            names.append(row[0])
    player: Player
    for player in player_list:
        return True if not getattr(player, 'name') in names else False


def recordTurn(turn: int, player: Player) -> None:
    """
    Record the player's actions into the 'History' table.

    Parameters
    ----------
    turn : int
        Current round of the game.
    player : Player
        One of the game's players.
    """
    with connection:
        cursor.execute("INSERT INTO History VALUES (?, ?, ?)",
                       (turn, getattr(player, 'name'),
                        getattr(player, 'guess')))
        connection.commit()


def recordStats(turn: int, player: Player) -> None:
    """
    Record the player's actions into the 'History' table.

    Parameters
    ----------
    turn : int
        Current round of the game.
    player : Player
        One of the game's players.
    """
    with connection:
        cursor.execute("INSERT INTO Statistics VALUES (?, ?, ?, ?, ?)",
                       (turn, getattr(player, 'name'),
                        getattr(player, 'mean'), getattr(player, 'median'),
                        getattr(player, 'mode')))
        connection.commit()


def recordOutcome(turn: int, winner: Optional[str], gain: int) -> None:
    """
    Record the outcome of the turn to the 'Outcome' table.

    Parameters
    ----------
    turn : int
        Current round of the game.
    winner : Optional[str]
        Name of the turn's winner, if anyone won.
    gain : int
        Number of points the winner was awarded, if anyone won.
    """
    with connection:
        cursor.execute("INSERT INTO Outcome VALUES (?, ?, ?)",
                       (turn, winner, gain))
        connection.commit()


def saveGame(player: Player) -> None:
    """
    Save current game state to SQL database.

    Parameters
    ----------
    player : Player
        One of the game's players.
    """
    with connection:
        cursor.execute("INSERT INTO Save VALUES (?, ?, ?)",
                       (getattr(player, 'name'),
                        getattr(player, 'points'),
                        getattr(player, 'guess')))
        connection.commit()


def loadSave(player: Player) -> tuple[str, int, int]:
    """
    Load past save from SQL database.

    Parameters
    ----------
    player : Player
        One of the game's players.

    Returns
    -------
    tuple[str, int, int]
        The saved name, points, & guess of a player.
    """
    with connection:
        cursor.execute("SELECT * FROM Save WHERE name = ?",
                       (getattr(player, 'name'),))
        return cursor.fetchone()


def setStartingTurn() -> int:
    """
    Set starting turn number based off of last turn recorded.

    Returns
    -------
    int
        Turn number at which the last recorded game stopped.
    """
    with connection:
        cursor.execute("SELECT MAX(turn) FROM Outcome")
        return cursor.fetchone()[0] + 1


def getResponses(turn: int) -> dict[str, int]:
    """
    Return names & guesses of each player during the specified turn.

    Parameters
    ----------
    turn : int
        Current round of the game.

    Returns
    -------
    responses : dict[str, int]
        Dictionary of players and their guesses for a given turn.
    """
    with connection:
        cursor.execute("""SELECT name, guess
                          FROM History WHERE turn = ?""", (turn,))
        data: list[tuple[str, int]] = cursor.fetchall()

        # Transform list of tuples into dictionary (since it is sortable)
        responses: dict[str, int] = {}
        row: tuple[str, int]
        for row in data:
            player: str
            guess: int
            column: int
            for column in range(2):
                cell: Union[str, int] = row[column]
                if column == 0:
                    player = str(cell)  # Type cast for mypy
                elif column == 1:
                    guess = int(cell)  # Type case for mypy
            responses[player] = guess

        return responses


def getWinner(guess: int, turn: int) -> str:
    """
    Identify which player had the winning guess of the turn.

    Parameters
    ----------
    guess : int
        Guessed number that won the turn.
    turn : int
        Turn in which the winning number was guessed.

    Returns
    -------
    name : str
        Name of player who won the given turn.
    """
    with connection:
        # TODO: deselect turn & guess to simplify code
        cursor.execute("""SELECT name, turn, guess FROM History
                          WHERE guess = ? AND turn = ?""",
                       (guess, turn))
        name: str
        _: Any  # TODO: find out if there is a more type-safe/agnostic alternative
        name, *_ = cursor.fetchone()
        return name


def clearTable(table: str) -> None:
    """
    Clear values from specified table.

    Parameters
    ----------
    table : str
        Name of the table thats data will be cleared.
    """
    with connection:
        cursor.execute(f"DELETE FROM {table}")
        connection.commit()
        cursor.execute("VACUUM")


def main() -> None:
    """Allow user to clear the database of all values."""
    valid: bool = False
    while not valid:
        choice: str = (input("Clear the database? [y/n] ").lower()).strip()
        if (choice == "y") or (choice == "yes"):
            for table in ["Save", "Players", "Outcome", "History"]:
                clearTable(table)
            valid = True
        else:
            sys.exit()


# Uncomment following if using SQLite to calculate mean, median, & mode.
# cursor.execute("SELECT load_extension('libsqlitefunctions')")
file_name: str = "game_data.db"
loaded: bool = False
while not loaded:
    connection: sqlite3.Connection
    cursor: sqlite3.Cursor
    if os.path.isfile(file_name):
        connection, cursor = loadDatabase()
        print("Database loaded.")
        loaded = True
    else:
        print("Error: game database not found. Creating one now...")
        createDatabase()

if __name__ == "__main__":
    main()