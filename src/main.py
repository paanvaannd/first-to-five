#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""First to Five game initialization & core program flow."""

# Import built-in libraries
import sys
from typing import Optional

# Import custom modules
# import ai
import config
import database as db
import game
import players
import tables

player_list: list[players.Player] = players.initPlayers()


def play() -> Optional[str]:
    """
    Begin round of play between opponents.

    Returns
    -------
    victor : Optional[str]
        Name of the winning player.
    """
    _turn = game.fetchTurn()
    number_of_players = len(player_list)
    for player in player_list:
        _guess: int
        if getattr(player, 'species') == "human":
            _guess = game.humanTurn(player)
        else:
            _guess = game.computerTurn(player, number_of_players)
        player.setGuess(_guess)
        db.recordTurn(_turn, player)
        players.setStats(player)
        db.recordStats(_turn, player)
        
    use_chains = True if len(player_list) > 2 else False
    winner, gain = game.compareGuesses(use_chains)
    db.recordOutcome(_turn, winner, gain)
    for player in player_list:
        if getattr(player, 'name') == winner:
            setattr(player, 'points',
                    getattr(player, 'points') + gain)
        victor = game.checkIfWinner(player)
        if victor:
            print(f"\n~~{victor} is the victor!~~")
            tables.constructTable(player_list)
            config.toggleValue("welcomeMessage", "on")
            db.clearTable("Save")
            return victor


if not db.checkExists("Players"):
    for player in player_list:
        db.recreatePlayerTable(player)

        if getattr(player, 'species') == 'computer':
            players.setLevel(player)
else:
    changed = db.checkPlayerTable(player_list)
    for player in player_list:
        if changed:
            db.recreatePlayerTable(player)
        else:
            if db.checkExists("Statistics"):
                players.restoreStats(player)

        if getattr(player, 'species') == 'computer':
            players.setLevel(player)

if db.checkExists("Save"):
    valid = False
    while not valid:
        choice = (input("Load save data? [y/n] ").lower()).strip()
        if (choice == "y") or (choice == "yes"):
            for player in player_list:
                successful = players.restoreGame(player)
                if not successful:
                    print(f"Error: {getattr(player, 'name')}'s data absent.")
                    sys.exit()
            print("Save loaded.")
            valid = True
        elif (choice == "n") or (choice == "no"):
            print("Purging save data...")
            db.clearTable("Save")
            valid = True
        else:
            print("Please enter either \"(y)es\" or \"(n)o\".")

victor = None
autoPlay = config.checkBool("autoPlay")
if not autoPlay:
    if config.checkBool("welcomeMessage"):
        game.printWelcomeMessage()

    while victor is None:
        valid = False
        while not valid:
            choice = (input("\nWhat would you like to do? ").lower()).strip()

            if (choice == "play"):
                victor = play()
                valid = True

            elif (choice == "score"):
                tables.constructTable(player_list)
                valid = True

            elif (choice == "save"):
                if not hasattr(player_list[0], 'guess'):
                    print("Error: no current data to save.")
                else:
                    db.clearTable("Save")
                    for player in player_list:
                        db.saveGame(player)
                    print("Game saved.")
                    config.toggleValue("welcomeMessage", "off")
                    valid = True

            elif (choice == "help") or (choice == "?"):
                game.displayHelp()
                valid = True

            elif (choice == "quit") or (choice == "exit"):
                print("Goodbye!")
                sys.exit()

            else:
                print("Please enter a valid response. " +
                      "Enter \"help\" or \"?\" to see valid responses.")
else:
    print("Auto-play enabled. Script will exit when a computer wins.\n")
    while victor is None:
        victor = play()
        valid = True