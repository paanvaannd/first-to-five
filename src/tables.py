#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Pretty-print styled tables to stdout."""

# Import built-in libraries
from __future__ import annotations
from typing import Union

# Import custom modules
from players import Player

# Type aliases
TableHeaders = tuple[str, str, str]
PlayerAttrs = tuple[str, Union[str, int], int]
Row = Union[TableHeaders, PlayerAttrs]
Table = list[Union[TableHeaders, PlayerAttrs]]


def constructRow(player: Player) -> PlayerAttrs:
    """
    Specify Player attributes & order to report in table.

    Parameters
    ----------
    player : Player
        One of the game's players.

    Returns
    -------
    PlayerAttrs:
        Name, guess, & points of a player (if values are defined).
    """
    if not hasattr(player, 'guess'):
        return (getattr(player, 'name'),
                "N/A",
                getattr(player, 'points'))
    else:
        return (getattr(player, 'name'),
                getattr(player, 'guess'),
                getattr(player, 'points'))


def calculateColumnLength(*table: Row) -> list[int]:
    """
    Calculate longest length of each column's cells.

    Parameters
    ----------
    table : Table
        Name, guess, & points (if defined) of each player.

    Returns
    -------
    column_lengths : list[int]
        Contains each calculated longest column length per row.
    """
    column_lengths: list[int] = []
    headers: Row = table[0]
    number_of_columns: int = len(headers)
    column: int
    for column in range(number_of_columns):
        longest_length: int = 0
        row: Row
        for row in table:
            # TableHeaders & PlayerAttrs elements are either str or int
            cell: Union[str, int] = row[column]
            cell_length: int = len(str(cell))
            # Longest cell in column sets column length
            if cell_length > longest_length:
                longest_length = cell_length
        column_lengths.append(longest_length)
    return column_lengths


def printSeparator(num_of_dashes: int) -> None:
    """
    Visually separate each row.

    Parameters
    ----------
    num_of_dashes : int
        Number of dashes to print.
    """
    print(" ", end="", flush=True)
    _: int
    for _ in range(num_of_dashes - 1):
        print("-", end="", flush=True)
    print("-")


def printRow(column_lengths: list[int], row: Row) -> None:
    """
    Print row with cell data specified from passed arguments.

    Parameters
    ----------
    column_lengths : list[int]
        Contains each calculated longest column length per row.
    row : PlayerAttrs
    """
    print("|", end="", flush=True)
    column: int
    for column in range(len(row)):
        print(f" {row[column]} ", end="", flush=True)
        if len(str(row[column])) < column_lengths[column]:
            num_of_spaces = column_lengths[column] - len(str(row[column]))
            _: int
            for _ in range(num_of_spaces):
                print(" ", end="", flush=True)
        print("|", end="", flush=True)
    print()


def constructTable(player_list: list[Player]) -> None:
    """
    Print a formatted table showing current game statistics.

    Parameters
    ----------
    player_list : list
        List of Player instances, 1 per player defined in config.json.
    """
    data: Table = [("Name", "Last guess", "Points")]
    player: Player
    for player in player_list:
        data.append(constructRow(player))
    column_lengths: list[int] = calculateColumnLength(*data)

    printSeparator(sum(column_lengths) + 8)
    row: Row
    for row in data:
        printRow(column_lengths, row)
        printSeparator(sum(column_lengths) + 8)