#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Handles creation, loading, & modification of config.json."""

# Import built-in libraries
import fileinput
import json
import os
import sys
from typing import Optional, Union, TextIO

# Import 3rd-party libraries
import requests

# Type aliases
PlayerAttrs = dict[str, Union[str, int]]
ExistingPlayers = Optional[list[PlayerAttrs]]
JSONValue = Union[bool, ExistingPlayers]
JSONObject = dict[str, Union[bool, ExistingPlayers]]


def checkBool(value: str) -> bool:
    r"""Check JSON setting state for specified value.
    
    Parameters
    ----------
    value : str
        Name of JSON value whose state should be checked.

    Returns
    -------
    bool:
        True if JSON value is \"true\"; else False.
    """
    return True if config[value] else False


def toggleValue(value: str, state: str) -> None:
    r"""
    Toggle JSON value to the passed state.

    Parameters
    ----------
    value : str
        Name of JSON value whose state should be changed.
    state : str
        Desired state of JSON value (i.e. \"off\" or \"on\").
    """
    global config
    change_made: bool = False
    line: str
    for line in fileinput.input("config.json", inplace=True):
        if (f"\"{value}\": true,") in line and (state == "off"):
            line = line.replace("true", "false")
            change_made = True
        elif (f"\"{value}\": false,") in line and (state == "on"):
            line = line.replace("false", "true")
            change_made = True
        sys.stdout.write(line)
    if change_made:
        print(f"Toggled {value} {state}.")
        config = readConfig()
        print("Reloaded config.")


def getLists() -> tuple[JSONValue, JSONValue]:
    """
    Separate profiles in JSON file into human & computer player lists.

    Returns
    -------
    existing_species : tuple[ExistingPlayers, ExistingPlayers]
        Mapping of players to their respective species.
    """

    if "humans" in config:
        human_players: JSONValue = config["humans"]
    else:
        human_players = None
    if "computers" in config:
        computer_players: JSONValue = config["computers"]
    else:
        computer_players = None
    return (human_players, computer_players)


def readConfig() -> JSONObject:
    """
    Load a read/write JSON file containing program configuration parameters.

    Returns
    -------
    _config : JSONObject
        JSON object containing program parameters.
    """
    file_opened: bool = False
    while not file_opened:
        # TODO: standardize the file location relative to main.py's location
        if os.path.isfile("config.json"):
            file: TextIO
            with open("config.json", "r") as file:
                _config: JSONObject
                _config = json.load(file)
                # See: https://github.com/python/typing/issues/182
            file_opened = True
            return _config
        else:
            print("Error: config file not found. Creating one now...")
            target: str = \
                "https://gitlab.com/-/snippets/1946763/raw/master/config.json"
            response: requests.models.Response = requests.get(url=target)
            with open("config.json", "w") as file:
                file.write(response.text)
    print("Configuration loaded.")


def main() -> None:
    """Pretty-print JSON data (for debugging)."""
    print(json.dumps(getLists(), indent=2))


config = readConfig()

if __name__ == "__main__":
    main()