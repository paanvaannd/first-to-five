"""Specify exported modules & authorship information.

The object of the game is to reach 5 points before the other player.
The game is played by each player guessing a non-negative integer. The
player with the lower number gains 1 point. If both players guess the
same number, neither gets points. If the player guessing the higher
number guessed higher than the lower number by a value of exactly 1,
the player with the higher number gets 2 points.

The following modules are exported by this package:
- ai
- config
- database
- game
- main
- players
- tables
"""

# Authorship information
__author__      = "Pavan Anand"
__copyright__   = "Copyright 2020 Pavan Anand"
__credits__     = "Pavan Anand"
__license__     = "GPL-3.0-or-later"
__version__     = "0.8.0"
__maintainer__  = "Pavan Anand"
__email__       = "pavan.anand@pm.me"
__status__      = "Development"
