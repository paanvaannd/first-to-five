#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""This module handles the game logic."""

# Import built-in libraries
from __future__ import annotations
from typing import Optional, Union, NoReturn
import random
import sys

# Import custom modules
import database as db
import players
from players import Player

goal = 5
turn: int = db.setStartingTurn() if db.checkExists("Outcome") else 1


def displayHelp() -> None:
    """Display help message detailing available options."""
    print("""Your action choices are:
          - \"play\"
          - \"score\"
          - \"save\"
          - \"help\" or \"?\"
          - \"quit\" or \"exit\"""")


def printWelcomeMessage() -> None:
    """Display toggle-able welcome message."""
    print(f"""\nWelcome to \"First to Five!\"
          The rules of the game are as follows:
          1. First player to {goal} points wins.
          2. Take turns guessing numbers.
          3. Lowest number gets +1 pt.
            - Unless higher number is exactly 1 above the lowest number,
            in which case, player with the higher number gets +2 pts.
          4. If players guess the same number, +0 pts. for both.\n""")
    displayHelp()


def fetchTurn() -> int:
    """
    Export current turn number for reference, not modification.
    
    Returns
    -------
    int
        The current turn number.
    """
    return turn


def humanTurn(player: Player) -> int:
    """
    Prompt the human player for a guess.

    Parameters
    ----------
    player : Player
        One of the game's players.

    Returns
    -------
    response : int
        The number provided by a user as a guess on their turn.
    """
    valid = False
    while not valid:
        try:
            response: int = int(input("Guess a number: "))
            if (response < 0):
                raise ValueError
            else:
                return response
                valid = True
        except ValueError:
            print("Invalid response. " +
                  "Please enter either 0 or a positive integer.")


def computerTurn(player: Player,
                 number_of_players: int) -> Union[int, NoReturn]:
    """
    Generate AI guess based on its `level` JSON setting.

    Parameters
    ----------
    player : Player
        One of the game's players.
    number_of_players : int
        Number of player in the current round.

    Returns
    -------
    Union[int, NoReturn]
        The number provided by a computer as a guess on its turn.
    """
    if (getattr(player, 'level') == 1):
        return random.randint(0, (number_of_players + 1))
    # elif (getattr(player, 'level') == 2):
    # ...
    else:
        sys.exit("Error: please set a valid computer player level.")


# NOTE: Returns an Optional[str] as it could also be of type None.
def compareGuesses(use_chains: bool) -> tuple[Optional[str], int]:
    """
    Determine lowest- & highest-guessing player(s).

    Parameters
    ----------
    use_chains : bool
        True if more than 2 players; otherwise, False.

    Returns
    -------
    tuple[Optional[str], int]
        The winner of the turn & how many points they gained.
    """
    global turn
    responses: dict[str, int] = db.getResponses(turn)
    unsorted_guesses = list(responses.values())
    all_tied: bool = True
    test = unsorted_guesses[0]

    # Tests if all the guesses the same.
    for guess in unsorted_guesses:
        if guess != test:
            all_tied = False
            break
    if all_tied:
        winner = None
        gain = 0
        print("It's a draw because everone guessed the same number.")

    # If at least 1 guess is different from the rest:
    else:
        # Sort the responses by ascending order of guesses.
        responses = {k: v for k, v in sorted(responses.items(),
                                             key=lambda item: item[1])}
        print(responses)
        names = list(responses.keys())
        guesses = list(responses.values())

        # If the lowest guess is not the same as the 2nd lowest & lower
        # than it by at least 2...
        if (guesses[0] + 1 != guesses[1]) and (guesses[0] != guesses[1]):
            winner = names[0]
            gain = 1
            print(f"{winner} won {gain} points by guessing 2 lower than "
                  "the next highest guess.")

        # If there are more than 2 players...
        elif use_chains:
            # At this point, we know that index 1 is either equal to or 1
            # greater than index 0, so the null hypothesis (H0) is that it
            # is the winning guess.
            chain_winner = guesses[1]
            print(f"Assuming chain winner is {chain_winner}...")
            for number in guesses[2:]:  # Attempting to disprove H0...
                if number == chain_winner + 1:
                    chain_winner = number
            print("After running through the numbers, it appears that "
                   f"{chain_winner} is the winner.")
            if guesses.count(chain_winner) > 1:
                winner = None
                gain = 0
                print("It's a draw because at least 2 people guessed "
                      "the same highest number in the winning chain.")
            else:
                winning_index = guesses.index(chain_winner)
                winner = names[winning_index]
                if chain_winner == guesses[-1]:
                    gain = 2
                    print(f"{winner} won {gain} points by guessing "
                          "the highest number in the only chain.")
                else:
                    gain = 3
                    print(f"{winner} won {gain} points by guessing "
                          "the highest number in the lowest chain.")

        # If at least 2 players guessed the lowest number:
        elif guesses[0] == guesses[1]:
            winner = None
            gain = 0
            print("It's a draw because at least 2 people guessed "
                  "the same lowest number.")

        # Finally, if there are only 2 players, we've established that
        # the low-nunmber was 1-upped, so the higher number wins.
        else:
            winner = names[1]
            gain = 2
            print(f"{winner} won {gain} points by guessing exactly "
                  "1 number higher than the opponent.")
    print()  # DEBUG; makes reading each round's output easier.
    turn += 1
    return (winner, gain)


def checkIfWinner(player: Player) -> Optional[str]:
    """
    Determine if the player's score matches or exceeds the goal.

    Parameters
    ----------
    player : Player
        One of the game's players.

    Returns
    -------
    Optional[str]
        Name of the winning player.
    """
    if (score := getattr(player, 'points')) >= goal:
        if score > goal:
            players.correctOvershoot(player, goal)
        return getattr(player, 'name')
    else:
        return None