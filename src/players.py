#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Handles initiation & modification of the Player class."""

# Imports from standard library
from __future__ import annotations
from typing import Any
import sys
import os

# 3rd-party imports
import numpy as np
from scipy import stats

# Import local modules
import config
import database as db

player_list: list[Player] = []


class Player:
    r"""
    Stores attributes of each player.

    ...

    Attributes
    ----------
    name : str
        Name by which the player is referred.
    species : str
        \"Species\" of player (i.e. human or computer).
    guess : int
        Number which player last guessed.

    Methods
    -------
    setGuess(guess: int)
        Set `guess` attribute to last number player guessed.
    """

    points: int = 0

    def __init__(self, name: str, species: str) -> None:
        r"""
        Define player attributes & add player to list.

        Parameters
        ----------
        name : str
            Name by which the player is referred.
        species : str
            \"Species\" of player (i.e. human or computer).
        """
        self.name = name
        self.species = species
        player_list.append(self)

    def __repr__(self) -> str:
        """Player object initialization example."""
        return f"__class__.__name__(\"{self.name}\", \"{self.species}\")"

    def __str__(self) -> str:
        """Represent Player object as string."""
        return "{self.name}, a {self.species}, has {self.points} points."

    def setGuess(self, guess: int) -> None:
        r"""
        Set \"guess\" attribute to last number player guessed.

        Parameters
        ----------
        guess : int
            The player's most recently-guessed number, if one exists.
        """
        self.guess = guess


def setLevel(player: Player) -> None:
    """
    Set level of AI player based on its corresponding JSON profile.

    Parameters
    ----------
    player : Player
        One of the game's players.
    """
    profile: config.PlayerAttrs
    for profile in computer_players:
        if getattr(player, 'name') == profile["name"]:
            setattr(player, 'level', profile["level"])


def setStats(player: Player) -> None:
    """
    Set most recent guess mean, median, & mode.

    Parameters
    ----------
    player : Player
        One of the game's players.
    """
    data: np.ndarray[Any] = np.array(db.fetchData(getattr(player, 'name')))

    # Set mean
    # My implementation below is apparently faster than using numpy
    if not hasattr(player, 'mean'):
        setattr(player, 'mean', getattr(player, 'guess'))
    else:
        turn: int = db.setStartingTurn() if db.checkExists("Outcome") else 1
        _mean = ((turn - 1)                  \
                 * getattr(player, 'mean')   \
                 + getattr(player, 'guess')) \
                 / turn
        setattr(player, 'mean', round(_mean, 4))

    # Set median
    if not hasattr(player, 'median'):
        setattr(player, 'median', getattr(player, 'guess'))
    else:
        setattr(player, 'median', np.median(data))

    # Set mode
    if not hasattr(player, 'mode'):
        setattr(player, 'mode', getattr(player, 'guess'))
    else:
        _stats = stats.mode(data)
        _mode = int(_stats[0][0])
        setattr(player, 'mode', _mode)


def initPlayers() -> list[Player]:
    r"""
    Create players based on JSON profiles.

    Returns
    -------
    player_list : list[Player]
        A list of Player instances, 1 per player defined in \"config.json\".
    """
    try:
        profile: config.PlayerAttrs
        for profile in human_players:
            if isinstance(profile["name"], str):
                createPlayer(player_name=profile["name"],
                             player_species="human")
    except NameError:
        pass  # FIXME: Anything else to do to prevent or handle this?
    except TypeError:
        pass  # FIXME: Anything else to do to prevent or handle this?
    try:
        for profile in computer_players:
            if isinstance(profile["name"], str):
                createPlayer(player_name=profile["name"],
                             player_species="computer")
    except NameError:
        pass  # FIXME: Anything else to do to prevent or handle this?
    except TypeError:
        pass  # FIXME: Anything else to do to prevent or handle this?
    return player_list


def restoreStats(player: Player) -> None:
    """
    Set most recent database statistics as player attributes.

    Parameters
    ----------
    player : Player
        One of the game's players.
    """
    try:
        _: str
        _mean: float
        _median: float
        _mode: float
        _, _mean, _median, _mode = db.loadStats(player)
    except TypeError as error:
        print(f"System message: {error}")
        print("Program message: column pair asymmetry may exist; "
              "to fix, delete the database and try again")
        valid = False
        while not valid:
            choice = (input("Delete the database? [y/n] ").lower()).strip()
            if (choice == "y") or (choice == "yes"):
                try:
                    os.remove("game_data.db")
                    print("Database deleted.")
                except OSError:
                    print("Unable to find/remove database.")
                valid = True
            elif (choice == "n") or (choice == "no"):
                print("Database left undeleted.")
                valid = True
            else:
                print("Please enter either \"(y)es\" or \"(n)o\".")
        sys.exit()
    setattr(player, 'mean', _mean)
    setattr(player, 'median', _median)
    setattr(player, 'mode', _mode)


def createPlayer(player_name: str, player_species: str) -> Player:
    r"""
    Create an instance that stores player information.

    Parameters
    ----------
    player_name : str
        Name of the player.
    player_species : str
        Player \"species\" (i.e. human or computer).
    
    Returns
    -------
    Player
        Object representing a player of the game; holds player's attributes.
    """
    return Player(name=player_name, species=player_species)


def restoreGame(player: Player) -> bool:
    """
    Set most recent game data database values as player attributes.

    Parameters
    ----------
    player : Player
        One of the game's players.

    Returns
    -------
    bool
        Returns True if player stats restored; returns False otherwise.
    """
    try:
        _: str
        _points: int
        _guess: int
        _, _points, _guess = db.loadSave(player)
        setattr(player, 'points', _points)
        setattr(player, 'guess', _guess)
        return True
    except TypeError as error:
        print(f"Interpreter error: {error}")
        print("Program error: You may be passing too few or too many data "
              "while loading a player's data.")
        return False


def correctOvershoot(player: Player, goal: int) -> None:
    """
    Adjust winning player score down to goal if it was overshot.

    Parameters
    ----------
    player : Player
        One of the game's players.
    goal : int
        The score at which a player is declared the winner.
    """
    setattr(player, 'points', goal)


human_players: config.ExistingPlayers
computer_players: config.ExistingPlayers
human_players, computer_players = config.getLists()
if human_players:
    config.toggleValue("autoPlay", "off")
else:
    print("No human players specified.")
    config.toggleValue("autoPlay", "on")

if not computer_players:
    print("No computer players specified.")