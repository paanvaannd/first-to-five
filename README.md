# First to Five
Inspired by a [Real Python programming challenge](https://realpython.com/python-programming-contest-first-to-five/).

## Game rules

There are 2 variations to this game's rules. The 1<sup>st</sup> ruleset applies to games in which only 2 players are involved. The 2<sup>nd</sup> applies for games with more than 2 players.

### Ruleset 1

This ruleset applies to games with only 2 players.

1. The 1<sup>st</sup> player to 5 points wins.
2. Players take turns guessing numbers.
3. Points are awarded to players based off of their guesses:
    * if both players guessed the same number, neither player earns points.
    * if one player guessed a number ≥2 digits below the other player, they are awarded +1 point.
    * if one player guessed a number exactly 1 digit above the other player, they are awarded +2 points.
4. Loop until one player has earned ≥5 points.

#### Scoring logic

The art is to try to guess what the opponent will guess and go either 2 digits below that or precisely 1 digit above it.

Guessimg a low digit comes with the risk of giving up 2 points to the opponent; guessing high risks giving up 1 point. It may seem, therefore, that guessing high is the better choice as the risk involves only losing 1 point. However, the risk of guessing "too high" (i.e., more than 1 number above the low guess) is higher than the risk of guessing lower than the opponent by at least 2 digits.

Therefore, as guessing precisely 1 digit above the opponent is the riskier feat, players accomplishing this are awarded with 2 points.

### Ruleset 2

This ruleset applies to games with ≥2 players.

This ruleset makes use of guess "chains." A chain is a set of contiguous digits (e.g., `1, 2, 3` or `7, 8, 9`). There can be multiple chains in a single turn.

E.g.,

| Name    | Guess  |
|:--------|:------:|
| Alice   |   2    |
| Alfred  |   3    |
| Alfonse |   4    |
| Aloy    |   6    |
| Alonzo  |   7    |

*Table 1: An example of a turn in a game between 5 players showing each player's guess during that turn.*

In the above example, there are 2 chains: `2, 3, 4` and `6, 7`.

The ruleset of such a match between these 5 players would be as follows:

1. The 1<sup>st</sup> player to 5 points wins.
2. Players take turns guessing numbers.
3. Points are awarded to players based off of their guesses:
    * if all players guessed the same number, no players earn points.
    * regardless of how many chains are present, if one player guessed a number ≥2 digits below any other player, they are awarded +1 point.
    * if there is only 1 chain in play, if one player guessed a number exactly 1 digit above all other players in the chain, they are awarded +2 points.
    * if there are ≥2 chains in play, the player who guessed the highest number in the lowest chain is awarded +3 points.
4. Loop until one player has earned ≥5 points.

Going back to the example in *Table 1*, Alfonse would be awarded +3 points because he guessed the highest number (`4`) in the chain (`2, 3, 4`) that contained the set of digits of the lowest values (`2, 3, 4` < `6, 7`).

#### Scoring logic

The same logic from Ruleset 1 applies here concerning rules 1, 2, & 4. Certain subsets of rule 3 are also similar, such as the conditions of either all individuals guessing the same number or 1 individual guessing ≥2 digits below all others.

It is still safer to guess a lower digit than a higher one, so those who are able to guess the highest digit in the case of a single chain are awarded +2 points as a return on their risk. The riskier case is that in which an individual guesses the highest number in the lowest chain, as they had to "thread the needle" to both a) guess high enough to win one chain while b) not guessing high enough to become the lowest guess in the higher chain. As this is a much riskier tactic, they are awarded +3 points.

## Installation

Either download this repository in your preferred archive format ([zip](https://gitlab.com/paanvaannd/first-to-five/-/archive/master/first-to-five-master.zip) | [tar.gz](https://gitlab.com/paanvaannd/first-to-five/-/archive/master/first-to-five-master.tar.gz) | [tar.bz2](https://gitlab.com/paanvaannd/first-to-five/-/archive/master/first-to-five-master.tar.bz2) | [tar](https://gitlab.com/paanvaannd/first-to-five/-/archive/master/first-to-five-master.tar)) or clone the repository to your system using Git:

`$ git clone https://gitlab.com/paanvaannd/first-to-five.git <your/system/path> && cd <your/system/path>/first-to-five`

Replace `<your/system/path>` above with your desired installation directory's path.

## Environment setup
As this project has external dependencies, it is recommended to set up a virtual environment for the project to prevent clashes with the system Python version in case one exists.

To do so on Linux or macOS, I recommend using your existing virtual environment management solution or, if you find yourself without one so far:
1. Install & configure [`pyenv`](https://github.com/pyenv/pyenv) by following the project's instructions.
2. Install a version of Python that is ≥ version 3.9.6 (e.g., `$ pyenv install 3.9.6`).
3. Install & configure [`pyenv-virtualenv`](https://github.com/pyenv/pyenv-virtualenv) by following the project's instructions.
4. Create a virtualenv for this project (e.g. `$ pyenv virtualenv 3.9.6 first-to-five`).
  - *optional*: create a `.python-version` file with the contents of the virtualenv name to automatically activate the virtualenv upon `cd`-ing into the directory
5. Install package dependencies through `pip` (e.g. `$ pip install -r requirements-dev.txt`)
  - `requirements-min.txt` contains bare minimum external dependencies required to get the project running; this is useful for testing the game only without installing packages only useful for development
  - `requirements-dev.txt` contains the minimum advised development-related packages; this is agnostic to whether one uses Anaconda, PyCharm, VSCode, etc.
  - `requirements-vscode.txt` contains all packages from `requirements-dev.txt` along with a few that are specific to VSCode

If you are on Windows, either:
1. install WSL & follow the above Linux install instructions
2. check out the [`pyenv-win`](https://github.com/pyenv-win/pyenv-win) project.

## Usage
`$ cd <path/to/project/>src/main.py`

Run this from `src`'s parent directory to ensure that `game_database.db` is created in a location that is easy to clean up.

### Configuration

The program uses a JSON file, `config.json`, to list specifications for its runtime conditions. For example, you can modify the file to change the names of the players or (when the feature is available) set the difficulty of each AI player, if there are any.

If only AI players are specified, the program is set to auto-play. This is a nice feature to generate a large amount of data from which a machine learning algorithm can potentially learn or to demonstrate the collection, calculation, and visualization of statistics. These features are yet to come, but including the auto-play function is a reasonable first step towards developing these features and testing whether they are functioning properly in a development environment prior to deploying it to the main branch.

## Attribution
* GitLab project icon: *human intelligence vs artificial intelligence* by **Oleksandr Panasovskyi** (from [the Noun Project](https://thenounproject.com/))
